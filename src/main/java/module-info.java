module org.example {
    requires javafx.controls;
    requires com.fasterxml.jackson.databind;
    requires org.apache.commons.io;
    requires org.apache.commons.lang3;
    requires YahooFinanceAPI;
    requires org.slf4j;
    requires org.slf4j.simple;
    exports org.example;
}
