package org.example;

import javafx.application.Application;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

import java.io.FileOutputStream;
import java.io.ObjectOutputStream;

public class GameGUI extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception{
        primaryStage.setTitle("Game GUI");
        GridPane layout = new GridPane();

        creatLabels(layout);
        createFields(layout);
        createButton(layout);
        adjustLayout(layout);


        primaryStage.setScene(new Scene(layout, 500, 400));
        primaryStage.show();
    }

    private void adjustLayout(GridPane layout) {
        layout.setAlignment(Pos.CENTER);
        layout.setVgap(10);
        layout.setHgap(10);
    }

    private void createButton(GridPane layout) {
        Button save = new Button("Save");
        Button cancel = new Button("Cancel");
        layout.add(save,0,6);
        layout.add(cancel,1,6);
        GridPane.setHalignment(save, HPos.CENTER);
        GridPane.setHalignment(cancel, HPos.CENTER);
    }

    private void creatLabels(GridPane layout) {
        layout.add(new Label("Title"),0,0);
        layout.add(new Label("System"),0,1);
        layout.add(new Label("Dev"),0,2);
        layout.add(new Label("Category"),0,3);
        layout.add(new Label("Rating"),0,4);
        layout.add(new Label("Year"),0,5);

    }

    private void createFields(GridPane layout) {
        layout.add(new TextField("Title"),1,0);
        layout.add(new TextField("System"),1,1);
        layout.add(new TextField("Dev"),1,2);
        layout.add(new TextField("Category"),1,3);
        layout.add(new TextField("Rating"),1,4);
        layout.add(new TextField("Year"),1,5);
    }


    public static void main(String[] args) {
        launch(args);
    }
}
