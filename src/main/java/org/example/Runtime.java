package org.example;

import org.apache.commons.lang3.RandomUtils;

import java.util.ArrayList;

public class Runtime {

    public static void main(String[] args) {

        while(true) {
            int[] nums1 = getRandomNumberArray(40000);
            int[] nums2 = getRandomNumberArray(40000);
            long start = System.currentTimeMillis();
            ArrayList<Integer> commons = getCommonElements(nums1, nums2);
            long end = System.currentTimeMillis();
            System.out.println(end - start);
        }
    }

    private static int[] getRandomNumberArray(int size) {
        int[] num = new int[size];
        for (int i = 0; i < size; i++) {
            num[i] = RandomUtils.nextInt(0,size);
        }
        return num;
    }

    /***
     * takes two arrays and returns the common element in both arrays
     * @param nums1
     * @param nums2
     * @return
     */
    public static ArrayList<Integer> getCommonElements(int[] nums1, int[] nums2) {
        ArrayList<Integer> commons = new ArrayList<>();
        for (int i = 0; i < nums1.length; i++) {
            for (int j = 0; j < nums2.length; j++) {
                if (nums1[i] == nums2[j]) {
                    commons.add(nums1[i]);
                    continue;
                }
            }
        }
        return commons;
    }
}
