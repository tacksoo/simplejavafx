package org.example;


import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.io.IOUtils;

import java.io.IOException;
import java.net.URL;

/**
 * Represents a city with name, latitude and longitude
 *
 */
public class City {
    // needs latitude,longitude at the end of url
    public static final String DARKSKY_URL = "https://api.darksky.net/forecast/3c5084c558861c1610447b49a45f4eb4/";
    private String name;
    private double latitude;
    private double longitude;

    public City(String name, double latitude, double longitude) {
        this.name = name;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public double getCurrentTemperature() {
        ObjectMapper mapper = new ObjectMapper();
        String cityURL = DARKSKY_URL + latitude + "," + longitude;
        String tempString = "";
        try {
            URL url = new URL(cityURL);
            String json = IOUtils.toString(url.openStream(),"UTF-8");
            JsonNode root = mapper.readTree(json);
            JsonNode temp = root.get("currently").get("temperature");
            tempString = temp.asText();
        } catch(IOException e) {
            throw new RuntimeException("error retrieving weather data");
        }
        return Double.parseDouble(tempString);
    }
}

