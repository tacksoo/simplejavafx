package org.example;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.chart.BarChart;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.control.Label;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import yahoofinance.Stock;
import yahoofinance.YahooFinance;


/**
 * JavaFX App
 */
public class App extends Application {

    @Override
    public void start(Stage stage) throws Exception {
        City atlanta = new City("Atlanta", 33.7676931, -84.4906436);
        System.out.println(atlanta.getCurrentTemperature());
        City austin = new City("Austin", 30.3076863, -97.8934847);
        System.out.println(austin.getCurrentTemperature());
        City seattle = new City("Seattle", 47.6129432, -122.4821461);
        System.out.println(seattle.getCurrentTemperature());

        Stock gme = YahooFinance.get("NOK");
        System.out.println("Nokia is " + gme.getQuote().getPrice());

        drawBarChart(stage, atlanta, austin, seattle);
    }

    private void drawBarChart(Stage stage, City atlanta, City austin, City seattle) {
        CategoryAxis xAxis = new CategoryAxis();
        xAxis.setLabel("Cities");
        NumberAxis yAxis = new NumberAxis();
        yAxis.setLabel("Temperature (fahrenheit)");
        BarChart barChart = new BarChart(xAxis, yAxis);
        XYChart.Series dataSeries1 = new XYChart.Series();
        dataSeries1.setName("Temperature");
        dataSeries1.getData().add(new XYChart.Data("Atlanta",atlanta.getCurrentTemperature() ));
        dataSeries1.getData().add(new XYChart.Data("Austin", austin.getCurrentTemperature()));
        dataSeries1.getData().add(new XYChart.Data("Seattle",seattle.getCurrentTemperature()));
        barChart.getData().add(dataSeries1);
        VBox vbox = new VBox(barChart);
        Scene scene = new Scene(vbox, 400, 200);
        stage.setScene(scene);
        stage.setHeight(500);
        stage.setWidth(300);
        stage.show();
    }

    public static void main(String[] args) {
        launch();
    }

}