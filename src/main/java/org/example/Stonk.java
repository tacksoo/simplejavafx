package org.example;

import java.io.Serializable;

public class Stonk implements Serializable {
    private String ticker;
    private int shares;
    private double price;

    public Stonk(String ticker, int shares, double price) {
        this.ticker = ticker;
        this.shares = shares;
        this.price = price;
    }

    @Override
    public String toString() {
        return "Stonk{" +
                "ticker='" + ticker + '\'' +
                ", shares=" + shares +
                ", price=" + price +
                '}';
    }

    public String getTicker() {
        return ticker;
    }

    public void setTicker(String ticker) {
        this.ticker = ticker;
    }

    public int getShares() {
        return shares;
    }

    public void setShares(int shares) {
        this.shares = shares;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }
}
