package org.example;

import javafx.application.Application;
import javafx.geometry.HPos;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;

public class StonkApp extends Application {

    private final int NUMBER_OF_MEME_STONKS = 4;
    private TextField[] stonkFields = new TextField[NUMBER_OF_MEME_STONKS];

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) {

        primaryStage.setTitle("Stonks App - Become a meme millionaire");
        GridPane layout = new GridPane();

        creatLabels(layout);
        createFields(layout);
        createButton(layout);
        adjustLayout(layout);

        primaryStage.setScene(new Scene(layout, 500, 400));
        primaryStage.show();
    }

    private void adjustLayout(GridPane layout) {
        layout.setAlignment(Pos.CENTER);
        layout.setVgap(10);
        layout.setHgap(10);
    }

    private void createButton(GridPane layout) {
        Button save = new Button("Save");
        Button cancel = new Button("Cancel");
        layout.add(save,0,6);
        layout.add(cancel,1,6);
        GridPane.setHalignment(save, HPos.CENTER);
        GridPane.setHalignment(cancel, HPos.CENTER);
    }

    private void creatLabels(GridPane layout) {
        layout.add(new Label("BB"),0,0);
        layout.add(new Label("AMC"),0,1);
        layout.add(new Label("NOK"),0,2);
        layout.add(new Label("GME"),0,3);
    }

    private void createFields(GridPane layout) {
        try {
            String line = FileUtils.readFileToString(new File("stonks.txt"), "UTF-8");
            String[] columns = line.split(",");
            for (int i = 0; i < columns.length; i++) {
                stonkFields[i] = new TextField(columns[i].split(":")[1]);
            }
        } catch(IOException e) {
            throw new RuntimeException("Something went wrong with IO");
        }

        layout.add(stonkFields[0],1,0);
        layout.add(stonkFields[1],1,1);
        layout.add(stonkFields[2], 1,2);
        layout.add(stonkFields[3], 1,3);
    }
}
