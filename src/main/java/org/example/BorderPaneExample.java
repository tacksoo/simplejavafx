package org.example;

import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public class BorderPaneExample extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception {
        BorderPane layout = new BorderPane();
        layout.setPadding(new Insets(5,5,5,5));
        Button yesButton = new Button("YES");
        yesButton.setPrefSize(100,100);
        VBox vbox = new VBox();
        Text label = new Text("This is some TEXT");
        vbox.getChildren().add(yesButton);
        vbox.getChildren().add(new Button("Maybe"));
        Button noButton = new Button("NO");
        layout.setLeft(vbox);
        layout.setRight(noButton);
        layout.setCenter(label);
        createRow(layout);
        Scene scene = new Scene(layout,500,300);
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    private void createRow(BorderPane layout) {
        // create a HBox with two button and add it to the bottom of the
        // layout (BorderPane)

    }

    public static void main(String[] args) {
        launch();
    }
}
