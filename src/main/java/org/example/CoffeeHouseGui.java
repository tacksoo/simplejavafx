package org.example;

import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Text;
import javafx.stage.Stage;

import java.text.DecimalFormat;

public class CoffeeHouseGui extends Application {

    private RadioButton[] coffeeType;
    private CheckBox[] condiments;
    private TextField[] muffinType;

    private void calculateBill() {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setHeaderText("Your Bill");
        double bill = 0;
        if (coffeeType[0].isSelected())
            bill = 3;
        if (coffeeType[1].isSelected())
            bill = 3;
        for (int i = 0; i < condiments.length; i++) {
            if (condiments[i].isSelected()) {
                bill += 0.25;
            }
        }
        for (int i = 0; i < muffinType.length; i++) {
            int num = Integer.parseInt(muffinType[i].getText());
            bill += (num * 2.25);

        }
        double fullprice = bill + (bill * 0.07);
        alert.setContentText("Your bill is $" + new DecimalFormat("0.00").format(bill) + "\n" + "Your bill with tax is $" + new DecimalFormat("0.00").format(fullprice));
        alert.showAndWait();
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        primaryStage.setTitle("Welcome to Nick's Coffee Shop");
        BorderPane borderPane = new BorderPane();

        Text leftText = new Text("Coffee");
        GridPane coffee = new GridPane();
        coffee.setPadding(new Insets(10, 10, 10, 10));
        coffee.setVgap(10);
        coffee.setAlignment(Pos.TOP_CENTER);
        coffeeType = new RadioButton[2];
        coffeeType[0] = new RadioButton("Regular");
        coffeeType[1] = new RadioButton("Decaf");
        ToggleGroup group = new ToggleGroup();
        group.getToggles().addAll(coffeeType[0], coffeeType[1]);
        coffee.add(leftText, 0, 0);
        coffee.add(coffeeType[0], 0, 1);
        coffee.add(coffeeType[1], 0, 2);

        Text centerText = new Text("Condiments");
        GridPane condimentsList = new GridPane();
        condimentsList.setPadding(new Insets(10, 10, 10, 10));
        condimentsList.setVgap(10);
        condimentsList.setAlignment(Pos.TOP_CENTER);
        condiments = new CheckBox[5];
        condiments[0] = new CheckBox("Cream");
        condiments[1] = new CheckBox("Sugar");
        condiments[2] = new CheckBox("Artificial Sweetener");
        condiments[3] = new CheckBox("Cinnamon");
        condiments[4] = new CheckBox("Caramel");
        condimentsList.add(centerText, 1, 0);
        condimentsList.add(condiments[0], 1, 1);
        condimentsList.add(condiments[1], 1, 2);
        condimentsList.add(condiments[2], 1, 3);
        condimentsList.add(condiments[3], 1, 4);
        condimentsList.add(condiments[4], 1, 5);

        Text rightText = new Text("Muffins");
        GridPane muffins = new GridPane();
        muffins.setPadding(new Insets(10, 10, 10, 10));
        muffins.setVgap(10);
        muffins.setHgap(5);
        muffins.setAlignment(Pos.TOP_CENTER);
        Label bb = new Label("Blueberry");
        Label cc = new Label("Chocolate chip");
        Label bn = new Label("Banana Nut");
        Label bran = new Label("Bran");
        muffinType = new TextField[4];
        muffinType[0] = new TextField("0");
        muffinType[1] = new TextField("0");
        muffinType[2] = new TextField("0");
        muffinType[3] = new TextField("0");
        muffins.add(rightText, 2, 0);
        muffins.add(bb, 2, 1);
        muffins.add(cc, 2, 2);
        muffins.add(bn, 2, 3);
        muffins.add(bran, 2, 4);
        muffins.add(muffinType[0], 3, 1);
        muffins.add(muffinType[1], 3, 2);
        muffins.add(muffinType[2], 3, 3);
        muffins.add(muffinType[3], 3, 4);

        GridPane buttons = new GridPane();
        Button calc = new Button("Calculate");
        buttons.add(calc, 1, 1);
        calc.setOnAction(e -> calculateBill());

        primaryStage.setScene(new Scene(borderPane, 450, 200));
        borderPane.setLeft(coffee);
        borderPane.setCenter(condimentsList);
        borderPane.setRight(muffins);
        borderPane.setBottom(buttons);
        primaryStage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }
}